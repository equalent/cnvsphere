import codecs
import os.path

from pystache import render

statuses = {
    0: "в очереди",
    1: "компиляция программы",
    2: "компиляция программы",
    3: "выполнение программы",
    4: "компиляция чекера",
    5: "выполнение чекера",
    6: "компиляция главного чекера",
    7: "выполнение главного чекера",
    8: "тестирование программы",
    11: "ошибка компиляции",
    12: "ошибка при выполнении",
    13: "превышено ограничение времени",
    14: "неверный вывод",
    15: "принято",
    17: "превышено ограничение памяти",
    19: "НАРУШЕНИЕ БЕЗОПАСНОСТИ!!!",
    20: "ошибка поставщика, повторите отправку и/или\
        свяжитесь с преподавателем"
}

# ограничение на ...

limits = {
    "lines": "количество строк",
    "length": "количество символов",
    "attempts": "количество попыток"
}


def locStatusCode(code):
    if(code in statuses):
        return statuses[code]
    else:
        return "неизвестный статус"


def isFinalStatus(code):
    return (code >= 11) and (code <= 20)

def locLimit(code):
    if(code in limits):
        return limits[code]
    else:
        return "неизвестное ограничение"

def renderTemplate(tName, tData, path):
    tfile = codecs.open(os.path.join(
        path, "data", "templates", str(tName) + ".txt"), 'r', encoding="utf-8")
    text = tfile.read()
    tfile.close()
    return render(text, tData)
