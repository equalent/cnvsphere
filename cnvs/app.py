
import os.path
from os.path import join as pjoin
from time import sleep

from yaml import load

import cnvs.canvas
import cnvs.sphere

try:
    from yaml import CLoader as Loader
except ImportError:
    from yaml import Loader


class App:
    def __init__(self, args, path):
        self.shouldShutdown = False
        self.path = path
        self.args = args
        self.dataPath = pjoin(path, "data")
        if(not os.path.isdir(self.dataPath)):
            raise NotADirectoryError()
        try:
            cfgFile = open(pjoin(self.dataPath, "config.yml"), 'r')
            self.config = load(
                cfgFile, Loader=Loader)
            cfgFile.close()
        except FileNotFoundError:
            print("Config file not found!")
            raise FileNotFoundError()
        self.config['path'] = self.path
        self.canvas = cnvs.canvas.Canvas(self.config)
        self.sphere = cnvs.sphere.Sphere(self.config)

    def shutdown(self, sig, frame):
        self.shouldShutdown = True

    def loop(self):
        if(self.shouldShutdown):
            print("Shutting down...")
            print("Finishing Sphere Engine pending tasks...")
            while(len(self.sphere.pendingSubs) > 0):
                self.sphere.processPendingSubs(self.canvas)
            print("Done, shutting down Sphere Engine and Canvas drivers...")
            del self.sphere
            del self.canvas
            print("さようなら!")
            return -1
        print("LOOP " + str(self.loopn))
        if(self.loopn % 100 == 0):
            try:
                cfgFile = open(pjoin(self.dataPath, "config.yml"), 'r')
                self.config = load(
                    cfgFile, Loader=Loader)
                self.config['path'] = self.path
                self.canvas.cfg = self.config
                self.sphere.cfg = self.config
                cfgFile.close()
            except FileNotFoundError:
                print("Config file not found!")
                raise FileNotFoundError()
            self.sphere.cfg = self.config
            self.canvas.cfg = self.config
        self.sphere.processPendingSubs(self.canvas)
        self.canvas.processNewSubmissions(self.sphere)
        self.loopn += 1
        return 0

    def run(self):
        if(len(self.args) < 2):
            print("Starting main loop...")
            res = 0
            self.loopn = 1
            while(res == 0):
                res = self.loop()
                sleep(1)
        elif(len(self.args) == 3):
            print("Processing single assignment...")
            self.canvas.processAssignment(
                self.sphere, self.args[1], self.args[2])
            while(len(self.sphere.pendingSubs) > 0):
                try:
                    self.sphere.processPendingSubs(self.canvas)
                except Exception:
                    print(self.sphere.pendingSubs)
        else:
            print("WTF")
