
import urllib.request

from sphere_engine import ProblemsClientV4

from cnvs.submission import Submission
from cnvs.util import isFinalStatus, locStatusCode, renderTemplate, locLimit

from winsound import Beep


class Sphere:
    def __init__(self, cfg):
        self.client = ProblemsClientV4(
            cfg['sphere']['key'], cfg['sphere']['endpoint'])
        sphereTestMsg = "You can use Sphere Engine Problems API."
        if(self.client.test()['message'] != sphereTestMsg):
            print("Cannot use Sphere Engine Problems API V4!")
            raise ConnectionError()
        self.pendingSubs = []
        self.cfg = cfg
        print("Sphere Engine successfully initialized!")

    def exceedLimit(self, canvasSub, limitCode, canvas, val):
        data = {
            "limit": locLimit(limitCode),
            "limitValue": val
        }
        print("LIMIT ON %s WAS EXCEEDED BY %s" %
              (limitCode.upper(), canvasSub.user['name']))
        canvas.gradeAndComment(canvasSub, 0, renderTemplate(
            "limit", data, self.cfg['path']))

    def verifyLimit(self, canvasSub, code, canvas):
        if("limitAttempts" in self.cfg['sphere']['problems'][canvasSub.assignment_id]):
            if(canvasSub.attempt > self.cfg['sphere']['problems'][canvasSub.assignment_id]['limitAttempts']):
                self.exceedLimit(canvasSub, "attempts", canvas,
                                 self.cfg['sphere']['problems'][canvasSub.assignment_id]['limitAttempts'])
                return False
        if("limitLines" in self.cfg['sphere']['problems'][canvasSub.assignment_id]):
            if(len(str(code).strip().splitlines()) > self.cfg['sphere']['problems'][canvasSub.assignment_id]['limitLines']):
                self.exceedLimit(canvasSub, "lines", canvas,
                                 self.cfg['sphere']['problems'][canvasSub.assignment_id]['limitLines'])
                return False
        if("limitLength" in self.cfg['sphere']['problems'][canvasSub.assignment_id]):
            if(len(str(code)) > self.cfg['sphere']['problems'][canvasSub.assignment_id]['limitLength']):
                self.exceedLimit(canvasSub, "length", canvas,
                                 self.cfg['sphere']['problems'][canvasSub.assignment_id]['limitLength'])
                return False
        return True

    def processNewSub(self, canvasSub, canvas):
        # Add new submission to Sphere Engine
        codeUrl = canvasSub.attachments[0]['url']
        code = urllib.request.urlopen(codeUrl).read()
        codeText = code.decode("utf-8")
        if(not self.verifyLimit(canvasSub, codeText, canvas)):
            return None
        sphereSub = self.client.submissions.create(
            self.cfg['sphere']['problems'][canvasSub.assignment_id]['code'],
            codeText,
            self.cfg['sphere']['problems'][canvasSub.assignment_id]['compiler'])
        self.pendingSubs.append(Submission(sphereSub['id'], canvasSub))
        print("SUBMISSION %i WAS SENT TO SPHERE ENGINE!" % sphereSub['id'])
        return sphereSub['id']

    def processPendingSubs(self, canvas):
        for sub in self.pendingSubs:
            data = self.client.submissions.get(sub.sphereId)
            status = data['result']['status']['code']
            if(isFinalStatus(status)):
                canvasSub = sub.canvasSub
                points = data['result']['score']

                stc = data['result']['testcases']
                if(stc != None):
                    testcases = []
                    for tc in stc:
                        testcases.append({
                            "number": tc['number'],
                            "status": locStatusCode(tc['status']['code']),
                            "time": str(tc['time'])
                        })
                else:
                    testcases = False
                textData = {
                    "id": sub.sphereId,
                    "status": locStatusCode(status),
                    "points": points,
                    "testcases": testcases
                }
                if(int(status) == 11):
                    textData['cmperror'] = self.client.submissions.getSubmissionFile(
                        sub.sphereId, "cmpinfo")
                if((data['result']['streams']['error'] is not None) and (int(data['result']['streams']['error']['size']) > 0)):
                    textData['error'] = self.client.submissions.getSubmissionFile(
                        sub.sphereId, "error")
                if("minPoints" in self.cfg['sphere']['problems'][canvasSub.assignment_id]):
                    if(points >= self.cfg['sphere']['problems'][canvasSub.assignment_id]['minPoints']):
                        points = "pass"
                    else:
                        points = "fail"
                canvas.gradeAndComment(sub.canvasSub, points, renderTemplate(
                    "statuschange", textData, self.cfg['path']))
                print("SPHERE ENGINE RETURNED STATUS '%s' FOR SUBMISSION %i" %
                      (data['result']['status']['name'].upper(), sub.sphereId))
                Beep(2000, 150)
                Beep(1500, 200)
                Beep(2000, 150)
                Beep(1500, 200)
                self.pendingSubs.remove(sub)
