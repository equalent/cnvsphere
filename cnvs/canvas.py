
from canvasapi import Canvas as CCanvas
from cnvs.util import renderTemplate
from winsound import Beep

class Canvas:
    def __init__(self, cfg):
        self.cfg = cfg
        self.client = CCanvas(cfg['canvas']['endpoint'], cfg['canvas']['key'])
        self.subsDone = []

    def processAssignment(self, sphere, courseId, assId):
        del self.client
        self.client = CCanvas(
            self.cfg['canvas']['endpoint'], self.cfg['canvas']['key'])
        ass = self.client.get_course(courseId).get_assignment(assId)
        for dsub in ass.get_submissions(include=['user']):
            print("NEW SUBMISSION FROM: " +
                  dsub.user['name'])
            dsub.mark_read()
            if(hasattr(dsub, 'attachments') and (len(dsub.attachments) < 1)):
                print(dsub.user['name'] +
                          " DID'T SEND ANYTHING. GRADING 0.")
                self.gradeAndComment(dsub, 0, renderTemplate(
                        "onnoattachment", None, self.cfg['path']))
            elif(hasattr(dsub, 'attachments')):
                fext = dsub.attachments[0]['filename'].strip().split(
                        ".")[-1].strip()
                if((fext not in self.cfg['allowedExtensions']) or (dsub.attachments[0]['content-type'] not in self.cfg['allowedMimes'])):
                    print(dsub.user['name'] +
                              " USED UNALLOWED FILE EXTENSION %s. GRADING 0." % fext)
                    self.gradeAndComment(dsub, 0, renderTemplate(
                            "onwrongextension", None, self.cfg['path']))
                else:
                    print("SENDING TO SPHERE ENGINE...")
                    sphereId = sphere.processNewSub(dsub, self)
                    if(sphereId is not None):
                        self.gradeAndComment(dsub, 0, renderTemplate(
                                "onsubmit", {"id": sphereId}, self.cfg['path']))

    def processNewSubmissions(self, sphere):
        del self.client
        self.client = CCanvas(
            self.cfg['canvas']['endpoint'], self.cfg['canvas']['key'])
        for dcourse in self.cfg['canvas']['courses']:
            course = self.client.get_course(dcourse['id'])
            for dsub in course.get_multiple_submissions(student_ids="all",
                                                        assignment_ids=dcourse['assignments'],
                                                        workflow_state="submitted", include="user"):
                if(dsub.id in self.subsDone):
                    continue
                print("NEW SUBMISSION FROM: " +
                      dsub.user['name'])
                dsub.mark_read()
                if(hasattr(dsub, 'attachments') and (len(dsub.attachments) < 1)):
                    print(dsub.user['name'] +
                          " DID'T SEND ANYTHING. GRADING 0.")
                    self.gradeAndComment(dsub, 0, renderTemplate(
                        "onnoattachment", None, self.cfg['path']))
                elif(hasattr(dsub, 'attachments')):
                    fext = dsub.attachments[0]['filename'].strip().split(
                        ".")[-1].strip()
                    if((fext not in self.cfg['allowedExtensions']) or (dsub.attachments[0]['content-type'] not in self.cfg['allowedMimes'])):
                        Beep(300, 500)
                        Beep(300, 500)
                        Beep(300, 500)
                        print(dsub.user['name'] +
                              " USED UNALLOWED FILE EXTENSION %s. GRADING 0." % fext)
                        self.gradeAndComment(dsub, 0, renderTemplate(
                            "onwrongextension", None, self.cfg['path']))
                        
                    print("SENDING TO SPHERE ENGINE...")
                    sphereId = sphere.processNewSub(dsub, self)
                    Beep(1500, 250)
                    Beep(1500, 250)
                    if(sphereId is not None):
                        self.gradeAndComment(dsub, 0, renderTemplate(
                            "onsubmit", {"id": sphereId}, self.cfg['path']))
                del dsub
            del course

    def comment(self, sub, message):
        ncomment = {
            "text_comment": str(message)
        }
        sub.edit(comment=ncomment)

    def grade(self, sub, points):
        nsub = {
            "posted_grade": str(points)
        }
        sub.edit(submission=nsub)

    def gradeAndComment(self, sub, points,
                        message):
        self.grade(sub, points)
        self.comment(sub, message)
