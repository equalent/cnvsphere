
import os.path as ppath
import signal
import sys

import cnvs.app

path = ppath.dirname(ppath.realpath(__file__))


def main():
    print("Copyright (c) 2019 Andrey Tsurkan. All rights reserved.")
    print("This software is released under Apache License 2.0\n")
    print("MD: " + path + "\n")
    args = sys.argv
    app = cnvs.app.App(args, path)
    signal.signal(signal.SIGINT, app.shutdown)
    app.run()


if __name__ == "__main__":
    main()
