== ИЗМЕНЕН СТАТУС ОТПРАВКИ ==
ОТПРАВКА: {{id}}
НОВЫЙ СТАТУС: {{status}}
{{#points}}
БАЛЛЫ: {{points}}
{{/points}}
{{#cmperror}}
ОШИБКА КОМПИЛЯЦИИ: {{cmperror}}
{{/cmperror}}
{{#runerror}}
ОШИБКА ВЫПОЛНЕНИЯ: {{runerror}}
{{/runerror}}
{{#comment}}
КОММЕНТАРИЙ: {{comment}}
{{/comment}}
{{#testcases}}
= ТЕСТ №{{number}} =
СТАТУС: {{status}}
ВРЕМЯ: {{time}} сек.
{{/testcases}}
ЭТО АВТОМАТИЧЕСКОЕ СООБЩЕНИЕ.
С ВОПРОСАМИ ОБРАЩАЙТЕСЬ К ПРЕПОДАВАТЕЛЮ.